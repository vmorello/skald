import os
import sys
import setuptools
import subprocess

from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext

# TODO: install versioneer
VERSION = '0.0.1'

with open("README.md", "r") as fh:
    long_description = fh.read()

install_requires = [
    'astropy',
    'numpy',
    'pandas',
    'matplotlib',
]

setup_requires = [
    'pybind11>=2.5.0'
]

setup(
    name='skald',
    version=VERSION,
    url='https://bitbucket.org/vmorello/skald/',
    author='Vincent Morello',
    author_email='vmorello@gmail.com',
    description='A proof-of-concept parallel candidate folding code for the SKA pulsar search',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    install_requires=install_requires,
    setup_requires=setup_requires,
    license='MIT License',

    # NOTE (IMPORTANT): This means that everything mentioned in MANIFEST.in will be copied at install time 
    # to the package’s folder placed in 'site-packages'
    include_package_data=True,

    entry_points = {
        'console_scripts': [
            'skald_benchmark=skald.apps.benchmark:main',
            'skald_benchmark_suite=skald.apps.benchmark_suite:main',
            'skald_fold=skald.apps.fold:main',
            'skald_gridfold=skald.apps.gridfold:main'
        ],
    },

    classifiers=[
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: C++",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Unix",
        "Topic :: Scientific/Engineering :: Astronomy"
        ],
)