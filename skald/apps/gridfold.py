import argparse
import json
import os
import numpy as np
import time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from skald.sigproc_utils import SigprocHeader, load_filterbank
from skald.libfold import TFBlock_uint8, Predictor, fold_uint8


def parse_args():
    parser = argparse.ArgumentParser(
        description="Fold a grid of candidates in nu - nudot space from a SIGPROC Filterbank file."
        " Saves 5-dimensional .npy file with the two-dimensional grid of folded data cubes. "
        " Also saves a plot with the sub-integrations of each candidates arranged in a grid.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--tsub", help="Subintegration length in seconds", 
        type=float, default=8.0)
    parser.add_argument(
        "--nu", help="Candidate frequency", type=float, default=1.0)
    parser.add_argument(
        "--nudot", help="Candidate frequency derivative", type=float, default=0.0)
    parser.add_argument(
        "--tref", help="Time at which the phase model parameters are valid. tref is measured"
        " from the start of the observation, in seconds. If unspecified, defaults to the midpoint"
        " of the observation.",
        type=float, default=None)
    parser.add_argument(
        "--dm", help="Candidate dispersion measure", type=float, default=0.0)
    parser.add_argument(
        "--nband", help="Candidate number of bands", type=int, default=64)
    parser.add_argument(
        "--nbin", help="Candidate number of phase bins", type=int, default=128)

    parser.add_argument(
        "--nusteps", help="Number of frequency steps *on each side*", type=int, default=4)
    parser.add_argument(
        "--nudotsteps", help="Number of frequency steps *on each side*", type=int, default=4)
    parser.add_argument(
        "--gridscale", help="Phase difference between two consecutive grid steps", 
        type=float, default=0.125)
    parser.add_argument(
        "--threads", help="Number of folding threads", 
        type=int, default=len(os.sched_getaffinity(0)))
    parser.add_argument("fname", help="SIGPROC filterbank file")
    return parser.parse_args()


def format_texsi(x, n):
    s = '{x:0.{n:d}e}'.format(x=x, n=n)
    m, e = s.split('e')
    return r'{m:s}\times 10^{{{e:d}}}'.format(m=m, e=int(e))


def main():
    import matplotlib.pyplot as plt
    args = parse_args()

    print("Reading file ...")
    start = time.time()
    input_data, sh = load_filterbank(args.fname)
    print(f"Done ({time.time() - start:.3f} seconds).")

    #######################################################
    ### Generate trial grid and predictors
    #######################################################
    nsamp, nchan = input_data.shape
    tobs = nsamp * sh['tsamp']
    nsub = max(int(tobs / args.tsub), 1)

    tref = args.tref
    if tref is None:
        tref = nsamp * sh['tsamp'] / 2.0

    # delta_nu * tobs = gridscale
    delta_nu = args.gridscale / tobs

    # 0.5 * delta_nudot * tobs^2 = gridscale
    delta_nudot = 2.0 * args.gridscale / tobs**2

    clight = 299792458.0
    acc = clight * args.nudot / args.nu
    delta_acc = clight * delta_nudot / args.nu

    nu_trials = args.nu + np.arange(-args.nusteps, args.nusteps+1) * delta_nu
    nudot_trials = args.nudot + np.arange(-args.nudotsteps, args.nudotsteps+1) * delta_nudot

    predictors = [
        Predictor(nsub, args.nband, args.nbin, args.dm, nu, nudot=nudot, tref=tref)
        for nudot in nudot_trials
        for nu in nu_trials
    ]

    for pred in predictors:
        print(pred)

    #######################################################
    ### Fold
    #######################################################
    print("Folding ...")
    start = time.time()
    block = TFBlock_uint8.from_ndarray(input_data, 0.0, sh['tsamp'], sh['fch1'], sh['foff'])
    block_samples = 4096
    candidates = fold_uint8(block, predictors, block_samples, args.threads)
    print(f"Done ({time.time() - start:.3f} seconds).")

    #######################################################
    ### Save data
    #######################################################
    np.save("gridfold_nudot.npy", nudot_trials)
    np.save("gridfold_nu.npy", nu_trials)

    data = []
    for cand in candidates:
        X = cand.data()
        nt, nf, __ = X.shape
        X -= X.mean(axis=-1).reshape(nt, nf, 1)
        scale = X.std(axis=-1).reshape(nt, nf, 1)
        scale[scale == 0] = 1.0
        X /= scale
        data.append(X)
    
    data = np.asarray(data).reshape(nudot_trials.size, nu_trials.size, nsub, args.nband, args.nbin)
    outfile = "gridfold_output.npy"
    np.save(outfile, data)
    print(f"Saved normalised data to {outfile!s}")

    #######################################################
    ### Plots
    #######################################################
    nx, ny, nsub, nband, nbin = data.shape
    plt.figure(figsize=(16, 16))
    subints = data.sum(axis=-2)
    for ii in range(nx):
        for jj in range(ny):
            plt.subplot(nx, ny, ii * ny + jj + 1)
            plt.imshow(subints[ii, jj], aspect='auto', cmap='Greys')
            plt.xticks([])
            plt.yticks([])

            # Highlight centre of grid
            if (ii == nx//2) and (jj == ny//2):
                ax = plt.gca()
                ax.patch.set_edgecolor('red')  
                ax.patch.set_linewidth(4)  

    plt.suptitle(
        fr"$T = {tobs:.2f}$ s"
        "\n"
        fr"$\nu = {args.nu:.6f}$ Hz    $\dot{{\nu}} = {format_texsi(args.nudot, 3)}$ Hz/s    $a = {format_texsi(acc, 2)}\,\mathrm{{m/s}}^{{2}}$"
        "\n"
        fr"$\Delta\nu = {format_texsi(delta_nu, 2)}$ Hz    $\Delta\dot{{\nu}} = {format_texsi(delta_nudot, 2)}$ Hz/s    $\Delta a = {format_texsi(delta_acc, 2)}\,\mathrm{{m/s}}^{{2}}$",
        fontsize=24
    )
    
    plt.subplots_adjust(left=0.01, right=0.99, bottom=0.01, top=0.89, wspace=0.05, hspace=0.05)
    plt.savefig("gridfold_subints.png")
    plt.close()


if __name__ == '__main__':
    main()