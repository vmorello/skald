import argparse
import json
import os
import numpy as np
import time

from skald.sigproc_utils import SigprocHeader, load_filterbank
from skald.libfold import TFBlock_uint8, Predictor, fold_uint8


def parse_args():
    parser = argparse.ArgumentParser(
        description="Fold SIGPROC Filterbank file. Saves a JSON file with candidate params and a "
        ".npy file with the data cube in the current working directory.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--tsub", help="Subintegration length in seconds", 
        type=int, default=8.0)
    parser.add_argument(
        "--nu", help="Candidate frequency", type=float, default=1.0)
    parser.add_argument(
        "--nudot", help="Candidate frequency derivative", type=float, default=0.0)
    parser.add_argument(
        "--tref", help="Time at which the phase model parameters are valid. tref is measured"
        " from the start of the observation, in seconds. If unspecified, defaults to the midpoint"
        " of the observation.",
        type=float, default=None)
    parser.add_argument(
        "--dm", help="Candidate dispersion measure", type=float, default=0.0)
    parser.add_argument(
        "--nband", help="Candidate number of bands", type=int, default=64)
    parser.add_argument(
        "--nbin", help="Candidate number of phase bins", type=int, default=128)
    parser.add_argument("fname", help="SIGPROC filterbank file")
    return parser.parse_args()


def params_dict(cand, fname):
    clight = 299792458.0
    p = cand.pred
    return {
        'fname': os.path.realpath(fname),
        'fch1': cand.freq_index.first,
        'df': cand.freq_index.step,
        'nchan': cand.freq_index.size,
        'tsub': cand.time_index.step,
        'nsub': cand.time_index.size,
        'nbin': cand.phase_index.size,
        'dm': p.dm,
        'nu': p.nu,
        'nudot': p.nudot,
        'acc': clight * p.nudot / p.nu
    }


def main():
    args = parse_args()

    print("Reading file ...")
    start = time.time()
    input_data, sh = load_filterbank(args.fname)
    print(f"Done ({time.time() - start:.3f} seconds).")

    print("Folding ...")
    start = time.time()
    nsamp, nchan = input_data.shape
    nsub = max(int((nsamp * sh['tsamp']) / args.tsub), 1)

    tref = args.tref
    if tref is None:
        tref = nsamp * sh['tsamp'] / 2.0

    pred = Predictor(nsub, args.nband, args.nbin, args.dm, args.nu, nudot=args.nudot, tref=tref)
    print(pred)
    block = TFBlock_uint8.from_ndarray(input_data, 0.0, sh['tsamp'], sh['fch1'], sh['foff'])

    block_samples = 4096
    threads = len(os.sched_getaffinity(0))
    candidate = fold_uint8(block, [pred], block_samples, threads)[0]
    print(f"Done ({time.time() - start:.3f} seconds).")

    with open("candidate_params.json", 'w') as fobj:
        fobj.write(json.dumps(params_dict(candidate, args.fname), indent=4))
    np.save(f"candidate_data.npy", candidate.data())


if __name__ == '__main__':
    main()