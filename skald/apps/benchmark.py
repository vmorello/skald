import argparse
import numpy as np
import os
from skald.benchmark_utils import benchmark


def parse_args():
    parser = argparse.ArgumentParser(
        description="Run CPU folding code benchmark for a single set of parameters, print the"
        " results to stdout.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    input_group = parser.add_argument_group("Input data parameters")
    input_group.add_argument(
        "--nsamp", help="Number of time samples in the input", type=int, default=2**20)
    input_group.add_argument(
        "--nchan", help="Number of frequency channels in the input", type=int, default=4096)
    input_group.add_argument(
        "--tau", help="Sampling interval in seconds", type=float, default=64e-6)
    input_group.add_argument(
        "--fch1", help="Frequency of first channel in MHz", type=float, default=1670.0)
    input_group.add_argument(
        "--bw", help="Total bandwidth in MHz", type=float, default=320.0)

    cand_group = parser.add_argument_group("Candidate parameters")
    cand_group.add_argument("--ncand", help="Number of candidates to fold", type=int, default=128)
    cand_group.add_argument(
        "--subint_samples", help="Number of time samples in a candidate subint", 
        type=int, default=2**17)
    cand_group.add_argument(
        "--nband", help="Number of frequency bands in the output candidates", type=int, default=64)
    cand_group.add_argument(
        "--numin", help="Minimum candidate frequency", type=float, default=1.0 / 23.0e-3)
    cand_group.add_argument(
        "--numax", help="Maximum candidate frequency", type=float, default=1.0 / 0.4e-3)
    cand_group.add_argument(
        "--dm", help="Dispersion measure for all candidates", type=float, default=10.0)
    cand_group.add_argument(
        "--binstrat", 
        help="Binning strategy, i.e. how the number of folding bin is chosen as a function of"
        " frequency for each candidate.",
        type=str, 
        choices=['origami', 'dspsr', 'nyquist'],
        default='origami'
    )

    perf_group = parser.add_argument_group("Performance parameters")
    perf_group.add_argument(
        "--block_samples", help="Process the input data in block with this number of time samples",
        type=int, default=4096)
    perf_group.add_argument(
        "--threads", 
        help="Number of threads to use. Defaults to the number of available logical cores", 
        type=int, default=len(os.sched_getaffinity(0)))
    perf_group.add_argument(
        "--bestof", 
        help="Run this number of trials and report the best result", 
        type=int, default=3)
    return parser.parse_args()


def main():
    args = parse_args()

    print("Invoked with")
    print("============")
    for key, val in vars(args).items():
        print(f"  {key:16s} {val}")

    result = benchmark(
        nsamp=args.nsamp, nchan=args.nchan, tau=args.tau, fch1=args.fch1, bw=args.bw, 
        ncand=args.ncand, subint_samples=args.subint_samples, nband=args.nband,
        binstrat=args.binstrat, dm=args.dm, numin=args.numin, numax=args.numax, 
        block_samples=args.block_samples, threads=args.threads, bestof=args.bestof
    )

    print("Results")
    print("=======")
    print(f"Additions: {result['num_adds'] / 1e9:.2f} G")
    print(f"Runtime  : {result['runtime']:.3f} s")
    print(f"Adds/s   : {result['aps'] / 1e9:.2f} G")


if __name__ == '__main__':
    main()
