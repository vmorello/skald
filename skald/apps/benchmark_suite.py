import os
import argparse
import logging
import pandas
import numpy as np
from skald.benchmark_utils import benchmark_grid

import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt


rcParams.update({
    'font.sans-serif': 'Lato',
    'font.size': 14,
    'axes.labelsize': 14,
    'grid.linestyle': ':',
    'legend.fontsize': 12,
    'legend.framealpha': 1.0
})


def parse_args():
    parser = argparse.ArgumentParser(
        description="Run the suite of benchmarks defined in AT4-524. Saves the output as a set of "
        "CSV files and generates summary PNG plots as well.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--maxthreads", help="Maximum number of folding threads. Defaults to the number of logical"
        " cores allocated to this process.", 
        type=int, default=len(os.sched_getaffinity(0))
    )
    parser.add_argument(
        "--outdir", 
        help="Write results in this directory. Defaults to the current working directory",
        type=str, default=os.getcwd()
    )
    return parser.parse_args()


def run_benchmark(**kwargs):
    results = benchmark_grid(**kwargs)
    df = pandas.DataFrame.from_dict(results)
    return df


def nchan_plot(df, ncand_default=128):
    df = df[df.ncand == ncand_default]
    nchan = df.nchan.unique()
    x = np.arange(len(nchan))

    bar_width = 0.25
    bar_offsets = {
        'nyquist': -bar_width,
        'dspsr': 0.0,
        'origami': +bar_width
    }
    bar_patches = {}

    fig = plt.figure(figsize=(8, 5), dpi=200)
    # NOTE: sort=False preserves the order in which binning strats appear in the data frame
    for binstrat, data in df.groupby('binstrat', sort=False):
        offset = bar_offsets[binstrat]
        bar_container = plt.bar(
            x + offset, data.aps / 1e9, width=bar_width, label=f"strat:{binstrat}"
        )
        bar_patches[binstrat] = bar_container.patches

    # Plot number of APS on each bar, but only after everything has been plotted, so that
    # ymin and ymax are now fixed
    ax = plt.gca()
    ymin, ymax = plt.ylim()
    for binstrat, rectangles in bar_patches.items():
        for rect in rectangles:
            height = rect.get_height()
            label = f"{height:.0f}"
            ax.text(
                rect.get_x() + rect.get_width() / 2, height - 0.08 * (ymax - ymin), label,
                ha='center', va='bottom', color='white', weight='bold'
            )

    ax.set_xticks(x)
    ax.set_xticklabels(nchan)
    plt.xlabel('Number of Channels')
    plt.ylabel(r'APS ($\times 10^{9}$)')
    plt.legend()
    plt.tight_layout()
    return fig


def ncand_plot(df, nchan_default=4096):
    df = df[df.nchan == nchan_default]
    ncand = df.ncand.unique()
    x = np.arange(len(ncand))

    bar_width = 0.25
    bar_offsets = {
        'nyquist': -bar_width,
        'dspsr': 0.0,
        'origami': +bar_width
    }
    bar_patches = {}

    fig = plt.figure(figsize=(12, 5), dpi=200)
    # NOTE: sort=False preserves the order in which binning strats appear in the data frame
    for binstrat, data in df.groupby('binstrat', sort=False):
        offset = bar_offsets[binstrat]
        bar_container = plt.bar(
            x + offset, data.aps / 1e9, width=bar_width, label=f"strat:{binstrat}"
        )
        bar_patches[binstrat] = bar_container.patches

    # Plot number of APS on each bar, but only after everything has been plotted, so that
    # ymin and ymax are now fixed
    ax = plt.gca()
    ymin, ymax = plt.ylim()
    for binstrat, rectangles in bar_patches.items():
        for rect in rectangles:
            height = rect.get_height()
            label = f"{height:.0f}"
            ax.text(
                rect.get_x() + rect.get_width() / 2, height - 0.08 * (ymax - ymin), label,
                ha='center', va='bottom', color='white', weight='bold'
            )

    ax.set_xticks(x)
    ax.set_xticklabels(ncand)
    plt.xlabel('Number of Candidates')
    plt.ylabel(r'APS ($\times 10^{9}$)')
    plt.legend()
    plt.tight_layout()
    return fig


def threads_plot(df):
    fig = plt.figure(figsize=(8, 5), dpi=200)

    # NOTE: sort=False preserves the order in which binning strats appear in the data frame
    for binstrat, data in df.groupby('binstrat', sort=False):
        plt.plot(data.threads, data.aps / 1e9, marker='o', label=f"strat:{binstrat}")

    ax = plt.gca()
    ax.set_xticks(df.threads)
    ax.set_xticklabels(df.threads)

    ymin, ymax = plt.ylim()
    plt.ylim(0, ymax)
    plt.xlim(df.threads.min(), df.threads.max())

    plt.ylabel(r'APS ($\times 10^{9}$)')
    plt.xlabel('Number of Threads')
    plt.title("APS versus $N_\\mathrm{threads}$")

    plt.grid()
    plt.legend()
    plt.tight_layout()
    return fig


def tobs_plot(df):
    df['tobs'] = df['nsamp'] * df['tau']
    fig = plt.figure(figsize=(8, 5), dpi=200)

    # NOTE: sort=False preserves the order in which binning strats appear in the data frame
    for binstrat, data in df.groupby('binstrat', sort=False):
        plt.plot(data.tobs, data.aps / 1e9, marker='o', label=f"strat:{binstrat}")

    ax = plt.gca()
    ax.set_xticks(df.tobs)
    ax.set_xticklabels([f"{t:.1f}" for t in df.tobs])

    plt.xlim(df.tobs.min(), df.tobs.max())
    plt.ylabel(r'APS ($\times 10^{9}$)')
    plt.xlabel('Observation Length (seconds)')
    plt.title("APS versus $T_\\mathrm{obs}$")

    plt.grid()
    plt.legend()
    plt.tight_layout()
    return fig


def main():
    args = parse_args()
    logging.basicConfig(level='DEBUG')
    logging.getLogger('matplotlib').setLevel('WARNING')
    os.chdir(args.outdir)

    ### (nchan, ncand) combinations
    df = run_benchmark(
        nchan=[1024, 2048, 4096],
        ncand=[32, 64, 128, 256, 512, 1024],
        binstrat=['nyquist', 'dspsr', 'origami'],
    )
    df.to_csv('benchmark_multiple.csv', index=False)
    nchan_plot(df).savefig(
        'benchmark_nchan_binstrat.png', dpi=200
    )
    ncand_plot(df).savefig(
        'benchmark_ncand_binstrat.png', dpi=200
    )

    ### Number of threads
    log2_maxthreads = int(np.log2(max(1, args.maxthreads)))
    threads_trialgrid = list(2 ** np.arange(0, log2_maxthreads + 1))

    df = run_benchmark(
        threads=threads_trialgrid, 
        binstrat=['nyquist', 'dspsr', 'origami'],
    )
    df.to_csv('benchmark_threads.csv', index=False)
    threads_plot(df).savefig(
        'benchmark_threads.png', dpi=200
    )

    ### Integration time
    df = run_benchmark(
        nsamp=[2**20, 2**21, 2**22, 2**23], 
        binstrat=['nyquist', 'dspsr', 'origami']
    )
    df.to_csv('benchmark_tobs.csv', index=False)
    tobs_plot(df).savefig(
        'benchmark_tobs.png', dpi=200
    )


if __name__ == '__main__':
    main()
