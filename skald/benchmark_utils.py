import os
import logging
import inspect
import itertools

import numpy as np
from skald.libfold import Predictor, benchmark_fold_uint8


log = logging.getLogger('skald.benchmark_utils')


def get_nbin(nu, tau, maxbins=128, binstrat='origami'):
    """ 
    Returns the number of phase bins to use for a given candidate frequency and data sampling time. 
    """
    if binstrat == 'origami':
        # This is the formula used in Origami, even though I have doubts about the leading factor 2
        # and the location of the int() cast
        nbin = 2 * int(1.0 / (nu * tau))
    elif binstrat == 'dspsr':
        nbin = int(1.0 / (nu * tau))
    elif binstrat == 'nyquist':
        nbin = int(0.5 / (nu * tau))
    else:
        raise ValueError(f"Invalid binning strategy {binstrat!r}")
    return min(maxbins, nbin)


def benchmark(nsamp=2**20, nchan=4096, tau=64e-6, fch1=1670.0, bw=320.0, ncand=128, 
    subint_samples=131072, nband=64, binstrat='origami', dm=10.0, numin=1/23.0e-3, numax=1/0.4e-3, 
    block_samples=4096, threads=len(os.sched_getaffinity(0)), bestof=3):
    """
    Benchmark the folding of a unsigned 8-bit time-frequency block in RAM, with the given 
    parameters. The function runs 'bestof' trials, and returns a dictionary containing the 
    parameters with which the function was called, and the benchmark result for the trial with
    the shortest run time.
    """
    # Store all parameters with which the function was called
    result = locals()
    log.debug(f"Running benchmark with parameters: {result}")

    # Fixed seed for reproducibility
    np.random.seed(0)

    if nchan % nband:
        raise ValueError("nband must divide nchan")

    # Draw candidate frequencies in a log-uniform distribution between numin and numax
    cand_logfreqs = np.random.uniform(np.log(numin), np.log(numax), ncand)
    cand_freqs = np.exp(cand_logfreqs)
    nsub = nsamp // subint_samples
    predictors = [
        Predictor(
            nsub, nband, get_nbin(nu, tau, binstrat=binstrat), 
            dm, nu, nudot=0.0, tref=0.0
        )
        for nu in cand_freqs
    ]

    runtimes = []
    for ii in range(bestof):
        log.debug(f"Running benchmark trial {ii+1}/{bestof} ...")
        num_adds, runtime = benchmark_fold_uint8(
            predictors, nsamp, nchan, tau, fch1, bw, block_samples, threads)
        runtimes.append(runtime)

    runtime = min(runtimes)
    result.update({
        'available_cpus': len(os.sched_getaffinity(0)),
        'runtime': runtime,
        'num_adds': num_adds,
        'aps': num_adds / runtime
    })
    log.debug(
        f'Best result: runtime = {runtime:.2f} seconds, APS = {num_adds / runtime / 1e9:.1f} G'
    )
    return result


def benchmark_grid(**kwargs):
    """
    Perform a benchmark over all possible permutations of the specified input parameters.
    This function expects only keyword arguments that are also keyword arguments of the benchmark()
    function. The value of each argument must be a list of all possible parameters values to try.
    Arguments that are NOT specified are set to the default value of the benchmark() function.
    Returns a list of dictionaries, each containing benchmark parameters and results.

    Example
    -------
    >>> benchmark_grid(nchan=[512, 1024], ncand=[64, 128], threads=[4])

    This would call:
        benchmark(nchan=512, ncand=64, threads=4)
        benchmark(nchan=512, ncand=128, threads=4)
        benchmark(nchan=1024, ncand=64, threads=4)
        benchmark(nchan=1024, ncand=128, threads=4)
    """
    params = inspect.signature(benchmark).parameters
    for key, val in kwargs.items():
        if not key in params:
            raise ValueError(f"{key!r} is not a parameter of the benchmark() function")
        if not isinstance(val, list):
            raise ValueError(f"The value of {key!r} must be a list of possible values")
        par = params[key]
        if par.kind not in {par.POSITIONAL_OR_KEYWORD, par.VAR_KEYWORD}:
            raise ValueError(f"{key!r} is not a keyword parameter of the benchmark() function")

    keys = list(kwargs.keys())
    value_lists = [kwargs[k] for k in keys]

    results = []
    for tup in itertools.product(*value_lists):
        benchmark_kwargs = {
            k : v
            for k, v in zip(keys, tup)
        }
        # Don't crash if one of the parameter values is not accepted by benchmark()
        try:
            results.append(benchmark(**benchmark_kwargs))
        except Exception as exc:
            log.exception(exc)
    return results
