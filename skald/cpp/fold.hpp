#ifndef FOLD_HPP
#define FOLD_HPP

#include <cstdint>
#include <vector>
#include <memory> // unique_ptr
#include <chrono>
#include <numeric> // iota
#include <iostream>
#include <omp.h>

#include "predictor.hpp"
#include "accumulator.hpp"
#include "tfblock.hpp"
#include "candidate.hpp"

/*
Fold a data block into multiple accumulators
*/
template <typename T>
std::vector<Candidate> fold(
    const TFBlock<T>& block, 
    const std::vector<Predictor>& predictors,
    size_t slice_samples,
    size_t num_threads) 
    {
    omp_set_num_threads(num_threads);

    std::vector<Accumulator> accumulators;
    std::vector<Candidate> cubes;

    // IMPORTANT: calling reserve is mandatory, otherwise the cube pointers stored by each
    // accumulator instance get invalidated as more cubes are pushed back in the loop below
    accumulators.reserve(predictors.size());
    cubes.reserve(predictors.size());

    for (auto pred : predictors)
        {
        cubes.emplace_back(pred, block.time_index(), block.freq_index());
        accumulators.emplace_back(pred, block.time_index(), block.freq_index(), &cubes.back());
        }

    for (size_t istart = 0; istart < block.nsamp(); istart += slice_samples)
        {
        TFBlock<T> slice = block.time_slice(istart, slice_samples);

        #pragma omp parallel for
        for (size_t icand = 0; icand < predictors.size(); ++icand)
            accumulators[icand].accumulate_block(slice);
        }
    
    #pragma omp parallel for
    for (size_t icand = 0; icand < accumulators.size(); ++icand)
        accumulators[icand].export_subint();

    return cubes;
    }


// Benchmark the whole folding operation. Returns a tuple (total_additions, runtime_seconds)
template <typename T>
std::tuple<double, double> benchmark_fold(
    const std::vector<Predictor>& predictors,
    size_t nsamp,
    size_t nchan,
    double tau,
    double fch1,
    double bw,
    size_t slice_samples,
    size_t num_threads) 
    {
    omp_set_num_threads(num_threads);
    const double df = -abs(bw) / nchan;

    std::unique_ptr<T[]> data(new T[nsamp * nchan]);
    std::iota(data.get(), data.get() + nsamp * nchan, 0);

    TimeIndex time_index = {0, tau, nsamp};
    FreqIndex freq_index = {fch1, df, nchan};
    TFBlock<T> block(data.get(), time_index, freq_index);

    auto start = std::chrono::high_resolution_clock::now();

    std::vector<Accumulator> accumulators;
    std::vector<Candidate> cubes;

    // IMPORTANT: calling reserve is mandatory, otherwise the cube pointers stored by each
    // accumulator instance get invalidated as more cubes are pushed back in the loop below
    accumulators.reserve(predictors.size());
    cubes.reserve(predictors.size());

    for (auto pred : predictors)
        {
        cubes.emplace_back(pred, block.time_index(), block.freq_index());
        accumulators.emplace_back(pred, block.time_index(), block.freq_index(), &cubes.back());
        }

    for (size_t istart = 0; istart < nsamp; istart += slice_samples)
        {
        TFBlock<T> slice = block.time_slice(istart, slice_samples);

        #pragma omp parallel for
        for (size_t icand = 0; icand < predictors.size(); ++icand)
            accumulators[icand].accumulate_block(slice);
        }

    #pragma omp parallel for
    for (size_t icand = 0; icand < accumulators.size(); ++icand)
        accumulators[icand].export_subint();

    auto end = std::chrono::high_resolution_clock::now();
    double time_seconds = std::chrono::duration<double>(end - start).count();
    double total_additions = nsamp * nchan * predictors.size();
    return std::make_tuple(total_additions, time_seconds);
    }


#endif // FOLD_HPP