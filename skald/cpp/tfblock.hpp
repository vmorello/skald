#ifndef TFBLOCK_HPP
#define TFBLOCK_HPP

#include <cstdint>
#include <algorithm>
#include "index.hpp"


template <typename T>
class TFBlock {

public:
    TFBlock(const T* data, TimeIndex ti, FreqIndex fi)
        : data_(data), time_index_(ti), freq_index_(fi)
        {}
    
    TimeIndex time_index() const {return time_index_;}
    FreqIndex freq_index() const {return freq_index_;}
    size_t nsamp() const {return time_index().size();}
    size_t nchan() const {return freq_index().size();}
    const T* sample_ptr(size_t i) const {return data_ + i * nchan();}
    double sample_timestamp(size_t i) const {return time_index()[i];}

    TFBlock time_slice(size_t start, size_t slice_samples) const {
        auto new_time_index = TimeIndex(
            sample_timestamp(start),
            time_index().step(),
            std::min(slice_samples, nsamp() - start)
        );
        return TFBlock(sample_ptr(start), new_time_index, freq_index());
    }

private:
    const T* data_;
    const TimeIndex time_index_;
    const FreqIndex freq_index_;

}; // class TFBlock


#endif // TFBLOCK_HPP