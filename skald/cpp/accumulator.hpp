#ifndef ACCUMULATOR_HPP
#define ACCUMULATOR_HPP

#include <cstdint>
#include <vector>
#include <memory>
#include <algorithm>
#include "predictor.hpp"
#include "tfblock.hpp"
#include "candidate.hpp"

// NOTE: The numeric type used for the counts buffer does impact performance measurably,
// using the shortest possible type (uint16_t) gives the best results
using acc_sums_buffer_t = uint16_t;
using acc_sums_t = float;
using acc_counts_buffer_t = uint16_t;
using acc_counts_t = uint32_t;

// Maximum number of samples that can go into a single sums buffer phase bin
// before there is a risk of saturation
constexpr size_t ACC_BIN_CAPACITY =  
    std::numeric_limits<acc_sums_buffer_t>::max() / std::numeric_limits<uint8_t>::max();


class Accumulator {

public:
    const Predictor pred() const
        {return pred_;}

    const FreqIndex freq_index() const
        {return freq_index_;}

    size_t nsub() const
        {return pred().nsub();}

    size_t nbin() const
        {return pred().nbin();}

    size_t nchan() const
        {return freq_index().size();}
    
    double tstart() const
        {return tstart_;}

    double tend() const
        {return tend_;}

    const std::vector<acc_sums_t>& sums() const
        {return sums_;}

    const std::vector<acc_counts_t>& counts() const
        {return counts_;}

    Accumulator(const Predictor& pred, TimeIndex scan_ti, FreqIndex scan_fi, Candidate* outcand)
        : pred_(pred)
        , freq_index_(scan_fi)
        , tstart_(scan_ti.first())
        , tend_(scan_ti.end())
        , isub_(0)
        , sums_buffer_()
        , counts_buffer_()
        , sums_()
        , counts_()
        , outcand_(outcand)
        {
        const size_t nchan = scan_fi.size();
        sums_buffer_.resize(nbin() * nchan);
        counts_buffer_.resize(nbin());
        sums_.resize(nbin() * nchan);
        counts_.resize(nbin());
        }

    template<typename T>
    void accumulate_block(const TFBlock<T>& block)
        {
        // TODO: check for contiguity with previous block
        for (size_t isamp = 0; isamp < block.nsamp(); ++isamp)
            accumulate_sample(block.sample_ptr(isamp), block.sample_timestamp(isamp));
        }

    // Add sums buffer into final sums, reset sums buffer to zero
    // Add counts buffer into final counts, reset counts buffer to zero
    void flush_buffers()
        {
        std::transform(
            sums_buffer_.begin(), sums_buffer_.end(), sums_.begin(), 
            sums_.begin(), std::plus<acc_sums_t>{}
            );
        std::fill(sums_buffer_.begin(), sums_buffer_.end(), 0);

        std::transform(
            counts_buffer_.begin(), counts_buffer_.end(), counts_.begin(), 
            counts_.begin(), std::plus<acc_counts_t>{}
            );
        std::fill(counts_buffer_.begin(), counts_buffer_.end(), 0);
        }

    // Export a finished sub-integration to the attached Candidate object
    void export_subint()
        {
        flush_buffers();

        // Normalise data by bin counts
        auto means = std::unique_ptr<float[]>(new float[nbin() * nchan()]);
        for (size_t ibin = 0; ibin < nbin(); ++ibin)
            {
            const acc_counts_t count = counts_[ibin];
            const float norm = count > 0 ? 1.0 / count : 1.0;
            acc_sums_t* in = &sums_[ibin * nchan()];
            float* out = means.get() + ibin * nchan();
            for (size_t ichan = 0; ichan < nchan(); ++ichan)
                out[ichan] = norm * in[ichan];
            }

        outcand_->ingest_accumulator_data(means.get(), freq_index(), isub_);
        std::fill(sums_.begin(), sums_.end(), 0);
        std::fill(counts_.begin(), counts_.end(), 0);
        ++isub_;
        }

protected:
    const Predictor pred_;
    const FreqIndex freq_index_;
    const double tstart_; // Start timestamp of observation to be folded
    const double tend_;   // End timestamp of observation to be folded
    size_t isub_;         // Index of subint being currently accumulated
    std::vector<acc_sums_buffer_t> sums_buffer_;
    std::vector<acc_counts_buffer_t> counts_buffer_;
    std::vector<acc_sums_t> sums_;
    std::vector<acc_counts_t> counts_;
    Candidate* const outcand_;

    size_t sample_isub(double t)
        {return (t - tstart()) / (tend() - tstart()) * nsub();}

    acc_sums_buffer_t* buffer_row_ptr(size_t ibin)
        {return &sums_buffer_[ibin * nchan()];}

    // Accumulate input sample containing nchan channels
    template<typename T>
    void accumulate_sample(const T* __restrict__ sample, double t)
        {
        if (sample_isub(t) > isub_)
            {
            export_subint();
            }

        const size_t ibin = pred().phase_bin(t);
        if (counts_buffer_[ibin] >= ACC_BIN_CAPACITY)
            {
            flush_buffers();
            }
        ++counts_buffer_[ibin];
        acc_sums_buffer_t* __restrict__ row = buffer_row_ptr(ibin);
        std::transform(sample, sample + nchan(), row, row, std::plus<acc_sums_buffer_t>{});
        }

}; // Accumulator


#endif // ACCUMULATOR_HPP