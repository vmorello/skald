#ifndef PREDICTOR_HPP
#define PREDICTOR_HPP

#include <cstddef>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>


class Predictor {

public:
    size_t nsub() const
        {return nsub_;}

    size_t nband() const
        {return nband_;}

    size_t nbin() const
        {return nbin_;}

    double dm() const
        {return dm_;}

    // Frequency at reference time tref
    double nu() const
        {return nu_;}

    // Frequency at arbitrary time t
    // We have: nu_at(tref) == nu()
    double nu_at(double t) const
        {return nu() + nudot() * (t - tref());}

    // Average frequency of the source over the time interval [t0, t1]
    double nu_mean(double t0, double t1) const
        {return 0.5 * (nu_at(t0) + nu_at(t1));}

    double nudot() const
        {return nudot_;}

    double tref() const
        {return tref_;}
    
    double phase(double t) const
        {
        const double u = t - tref();
        const double phi = nu() * u + 0.5 * nudot() * u * u;
        return phi - floor(phi);
        }

    size_t phase_bin(double t) const
        {return phase(t) * nbin();}

    std::string to_string() const
        {
        std::stringstream stream;
        stream << "Predictor("
            << "nsub=" << nsub()
            << ", nband=" << nband()
            << ", nbin=" << nbin()
            << std::fixed << std::setprecision(3)
            << ", dm=" << dm()
            << ", nu=" << nu()
            << ", nudot=" << std::scientific << nudot()
            << ", tref="  << std::fixed << tref()
            << ")";
        return stream.str();
        }

    Predictor(size_t nsub, size_t nband, size_t nbin, double dm, double nu, double nudot = 0.0, double tref = 0.0)
        : nsub_(nsub), nband_(nband), nbin_(nbin), dm_(dm), nu_(nu), nudot_(nudot), tref_(tref)
        {}

protected:
    const size_t nsub_;
    const size_t nband_;
    const size_t nbin_;

    // Phase model parameters, evaluated at time tref
    const double dm_;
    const double nu_;
    const double nudot_;

    // Reference time at which the phase model parameters are correct, expressed in seconds since
    // the start of the observation
    const double tref_;

}; // class Predictor


#endif // PREDICTOR_HPP
