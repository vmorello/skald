#ifndef KERNELS_HPP
#define KERNELS_HPP

#include <cstddef>
#include <memory>
#include <algorithm>
#include <cmath>

namespace kernels {

// Dispersion delay in seconds, for a source with dispersion measure dm,
// measured at frequency f with respect to frequency fch1
double dispersion_delay(double dm, double f, double fch1)
    {
    constexpr double kdm = 4149.377593360996;
    return kdm * dm * (pow(f, -2) - pow(fch1, -2));
    }


template <typename T>
void fused_rollback_accumulate(
    const T* __restrict__ input, 
    size_t size, 
    size_t shift,
    T* __restrict__ output)
    {
    const size_t p = shift % size;
    const size_t q = size - p;
    std::transform(input, input + p, output + q, output + q, std::plus<T>{});
    std::transform(input + p, input + size, output, output, std::plus<T>{});
    }


// Transpose, dedisperse, frequency scrunch, in that order.
template <typename T>
void trddfs(
    const T* __restrict__ input, // input 2D array in PF order, shape (nbin, nchan)
    size_t nbin,
    size_t nchan,
    size_t nband,
    double fch1, // Frequency of first input channel
    double df,   // Frequency offset between consecutive input channels
    double dm,   // Dispersion measure of the source
    double nu,   // average signal frequency of the source over the duration of the input data
    T* __restrict__ output) // output 2D array in FP order, shape (nband, nbin)
    {
    if (nchan % nband)
        throw std::invalid_argument("nband must divide nchan");

    const size_t chans_per_band = nchan / nband;
    std::unique_ptr<T[]> buffer(new T[nbin]);

    for (size_t iband = 0; iband < nband; ++iband)
        {
        const size_t chan_begin = iband * chans_per_band;
        const size_t chan_end = chan_begin + chans_per_band;
        T* outrow = output + iband * nbin;

        for (size_t ichan = chan_begin; ichan < chan_end; ++ichan)
            {
            // Gather profile at channel index ichan
            T* prof = buffer.get();
            for (size_t ibin = 0; ibin < nbin; ++ibin)
                prof[ibin] = input[ibin * nchan + ichan];

            const double f = fch1 + ichan * df;
            const double tdisp = dispersion_delay(dm, f, fch1);
            const size_t shift = round(tdisp * nu * nbin);

            fused_rollback_accumulate(prof, nbin, shift, outrow);
            }

        }

    }

} // namespace kernels

#endif // KERNELS_HPP