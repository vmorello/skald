#ifndef INDEX_HPP
#define INDEX_HPP

#include <cstddef>


enum class Unit {time, freq, phase};

template <typename T, Unit unit_id>
class Index {

public:
    typedef T numeric_type;
    static const Unit unit = unit_id;

    Index(T first, T step, size_t size)
        : first_(first), step_(step), size_(size)
        {}

    T first() const {return first_;}
    T step() const {return step_;}
    size_t size() const {return size_;}
    T span() const {return step() * size();}
    T operator[](size_t i) const {return first() + i * step();}
    T last() const {return this->operator[](size() - 1);}
    T end() const {return first() + span();}
    T centre() const {return 0.5 * (first() + last());}

private:
    const T first_;
    const T step_;
    const size_t size_;

}; // class Index


using TimeIndex  = Index<double, Unit::time>;
using FreqIndex  = Index<double, Unit::freq>;
using PhaseIndex = Index<double, Unit::phase>;

#endif // INDEX_HPP