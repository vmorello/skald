#ifndef CANDIDATE_HPP
#define CANDIDATE_HPP

#include <cstdint>
#include <cstring> // memcpy
#include <vector>
#include <algorithm>
#include "predictor.hpp"
#include "index.hpp"
#include "kernels.hpp"

// FIXME: Candidate precursor class for test purposes
// No transpose, no dedisperse, no scrunching for now
class Candidate {

public:
    Candidate(const Predictor& pred, TimeIndex scan_ti, FreqIndex scan_fi)
        : pred_(pred)
        , time_index_(subint_centre_timestamps(scan_ti, pred.nsub()))
        , phase_index_(0, 1.0 / pred.nbin(), pred.nbin())
        , freq_index_(subband_centre_freqs(scan_fi, pred.nband()))
        , data_()
        {
        const size_t size = pred.nsub() * pred.nbin() * scan_fi.size();
        data_.resize(size);
        }

    Predictor pred() const {return pred_;}
    TimeIndex time_index() const {return time_index_;}
    FreqIndex freq_index() const {return freq_index_;}
    PhaseIndex phase_index() const {return phase_index_;}

    size_t nsub() const {return time_index().size();}
    size_t nbin() const {return phase_index().size();}
    size_t nband() const {return freq_index().size();}

    const std::vector<float>& data() const {return data_;}

    // Ingest accumulator data (in PF order) into subint number isub
    void ingest_accumulator_data(const float* means, FreqIndex acc_fi, size_t isub)
        {
        const double tstart = time_index()[isub];
        const double tend = tstart + time_index().step();
        const double nu = pred().nu_mean(tstart, tend); // Average frequency over the subint
        kernels::trddfs(
            means, 
            nbin(), acc_fi.size(), nband(),
            acc_fi.first(), acc_fi.step(), pred().dm(), nu,
            subint_ptr(isub)
            );
        }

protected:
    const Predictor pred_;
    const TimeIndex time_index_;
    const PhaseIndex phase_index_;
    const FreqIndex freq_index_;
    std::vector<float> data_; // data in TFP order

    static TimeIndex subint_centre_timestamps(TimeIndex scan_ti, size_t nsub)
        {
        double tsub = scan_ti.span() / nsub;
        double tfirst = scan_ti.first() + tsub / 2.0;
        return TimeIndex(tfirst, tsub, nsub);
        }

    static FreqIndex subband_centre_freqs(FreqIndex scan_fi, size_t nband)
        {
        size_t nchan = scan_fi.size();
        if (nchan % nband)
            throw std::invalid_argument("nband must divide nchan");
        size_t chans_per_band = nchan / nband;
        double fstep = scan_fi.step() * chans_per_band; // Width of a subband
        double ffirst = scan_fi.first() - 0.5 * scan_fi.step() + 0.5 * fstep; // Centre freq of first subband
        return FreqIndex(ffirst, fstep, nband);
        }
    
    float* subint_ptr(size_t i) {return &data_[i * nbin() * nband()];}

}; // Candidate


#endif // CANDIDATE_HPP