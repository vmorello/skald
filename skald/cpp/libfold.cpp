#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <memory> // unique_ptr
#include <stdexcept>

#include "tfblock.hpp"
#include "predictor.hpp"
#include "accumulator.hpp"
#include "fold.hpp"
#include "index.hpp"
#include "candidate.hpp"


namespace py = pybind11;


// See: https://stackoverflow.com/questions/47487888/pybind11-template-class-of-many-types
template<typename T>
void declare_tfblock(py::module &m, const std::string& typestr) {
    using Class = TFBlock<T>;
    std::string pyclass_name = std::string("TFBlock_") + typestr;
    std::string docstring = 
        "Time-Frequency data block class; use from_ndarray() static method to create an instance";

    std::string from_ndarray_docstring = R"(Create a new TFBlock instance that wraps an existing numpy array

Parameters
----------
arr: ndarray
    Two-dimensional numpy array containing the data in TF order, i.e. shape (nsamp, nchan)
tstart: float
    Start time in seconds measured from an arbitrary reference point; the meaning of t = 0 is left 
    to the user. In most cases one should use tstart = 0, i.e. define the reference point as the
    start of the observation.
dt: float
    Time sampling interval in seconds
fch1: float
    Centre frequency of first channel in MHz. Note that in SIGPROC files, this is the highest
    channel frequency.
df: float
    Frequency offset from one channel to the next in the data. Note that in SIGPROC files, 
    df is NEGATIVE.
)";

    py::class_<Class>(m, pyclass_name.c_str(), docstring.c_str())
    .def_property_readonly("nsamp", &Class::nsamp)
    .def_property_readonly("nchan", &Class::nchan)
    .def_property_readonly("time_index", &Class::time_index)
    .def_property_readonly("freq_index", &Class::freq_index)

    // Returns a *copy* of the sums as a numpy array
    .def(
        "data", 
        [] (const Class& self) {
            return py::array_t<T, py::array::c_style>(
                {self.nsamp(), self.nchan()}, self.sample_ptr(0)
                );
            }
    )
    // Python-only constructor, from existing numpy array
    .def_static(
        "from_ndarray",
        [] (py::array_t<T> arr, double tstart, double dt, double fch1, double df) {
            auto buf = arr.request();
            if (!(buf.ndim == 2))
                throw std::invalid_argument("Input array must have 2 dimensions");

            const T* ptr = static_cast<const T*>(arr.request().ptr);
            return std::unique_ptr<Class>(new Class(
                ptr, 
                TimeIndex(tstart, dt, arr.shape(0)), 
                FreqIndex(fch1, df, arr.shape(1))
                ));
        },
        py::arg("arr"), py::arg("tstart"), py::arg("dt"), py::arg("fch1"), py::arg("df"),
        from_ndarray_docstring.c_str()
    );
}


template<typename T, Unit unit>
void declare_index_class(py::module &m, const std::string& pyclass_name)
    {
    using Class = Index<T, unit>;
    py::class_<Class>(m, pyclass_name.c_str())
    .def(
        py::init<T, T, size_t>(), 
        py::arg("first"), py::arg("step"), py::arg("size"),
        "Create a new Index instance"
    )
    .def_property_readonly("first", &Class::first)
    .def_property_readonly("step", &Class::step)
    .def_property_readonly("size", &Class::size)
    .def_property_readonly("span", &Class::span)
    .def_property_readonly("last", &Class::last)
    .def_property_readonly("end", &Class::end)
    .def_property_readonly("centre", &Class::centre)
    .def("__getitem__", &Class::operator[]);
    }


template <typename T>
void declare_fold(py::module &m, const std::string& block_type) 
    {
    std::string func_name = std::string("fold_") + block_type;
    std::string docstring = R"(Fold multiple candidates in parallel.

Parameters
----------
tfblock: TFBlock
    TFBlock instance with data type matching the name of this function.
predictors: list
    List of Predictor objects specifying the candidate parameters to fold
block_samples: int
    Process the data in blocks with this many time samples. Tweaking this parameter can result in
    faster execution times. A default value of 4096 is recommended.
threads: int
    Number of threads to use.

Returns
-------
candidates: list
    List of Candidate objects, each containing the 3-dimensional folded data.
    )";

    m.def(
        func_name.c_str(), 
        &fold<T>,
        py::arg("tfblock"), py::arg("predictors"), py::arg("block_samples"), py::arg("threads"),
        docstring.c_str()
        );
    }


template <typename T>
void declare_benchmark_fold(py::module &m, const std::string& block_type) 
    {
    std::string func_name = std::string("benchmark_fold_") + block_type;
    std::string docstring = R"(Benchmark the speed of the whole folding operation on a TF block with arbitrary
dimensions. The block is filled with values starting at 0 and increasing by 1 from one element to 
the next. The measured runtimes include the allocation of intermediate and output data products,
and the folding itself.

Parameters
----------
predictors: list
    List of Predictor objects specifying the candidate parameters to fold
nsamp: int
    Number of time samples in the input data
nchan: int
    Number of channels in the input data
dt: float
    Time sampling interval in seconds
fch1: float
    Centre frequency of first channel in MHz.
bw: float
    Bandwidth in MHz.
block_samples: int
    Process the data in blocks with this many time samples. Tweaking this parameter can result in
    faster execution times. A default value of 4096 is recommended.
threads: int
    Number of threads to use.

Returns
-------
additions: int
    Total number of additions performed (nsamp x nchan x ncand)
runtime: float
    Total run time in seconds
    )";
    m.def(
        func_name.c_str(), 
        &benchmark_fold<T>, 
        py::arg("predictors"), py::arg("nsamp"), py::arg("nchan"), py::arg("dt"), py::arg("fch1"), 
        py::arg("bw"), py::arg("block_samples"), py::arg("threads"),
        docstring.c_str()
        );
    }


PYBIND11_MODULE(libfold, m) {
    m.doc() = "libfold python bindings"; // optional module docstring

    std::string predictor_docstring = R"(Create a new Predictor instance. A Predictor defines the phase model parameters 
and dimensions of a Candidate to be folded.
    
Parameters
----------
nsub: int
    Number of sub-integrations
nband: int
    Number of frequency bands
nbin: int
    Number of phase bins
dm: float
    DM in pc/cm^3
nu: float
    Frequency in Hz
nudot: float
    Frequency derivative in Hz/s
tref: float
    Reference time at which the phase model parameters are valid, in seconds. What a reference time
    of tref = 0 means is left to the user. A Predictor's tref and a TFBlock's tstart are placed on
    the same time axis. If using a Predictor with tref = 300 to fold a TFBlock with a tstart = 0 
    and a length of 600 seconds, then the Predictor's phase parameters would be given at the
    midpoint the observation.
    )";

    py::class_<Predictor>(m, "Predictor")
        .def(
            py::init<size_t, size_t, size_t, double, double, double, double>(), 
            py::arg("nsub"), py::arg("nband"), py::arg("nbin"), py::arg("dm"), py::arg("nu"), 
            py::arg("nudot") = 0.0, py::arg("tref") = 0.0,
            predictor_docstring.c_str()
        )
        .def_property_readonly("nsub", &Predictor::nsub)
        .def_property_readonly("nband", &Predictor::nband)
        .def_property_readonly("nbin", &Predictor::nbin)
        .def_property_readonly("dm", &Predictor::dm)
        .def_property_readonly("nu", &Predictor::nu)
        .def_property_readonly("nudot", &Predictor::nudot)
        .def_property_readonly("tref", &Predictor::tref)
        .def("nu_at", &Predictor::nu_at)
        .def("nu_mean", &Predictor::nu_mean)
        .def("phase", &Predictor::phase)
        .def("phase_bin", &Predictor::phase_bin)
        .def("__str__", &Predictor::to_string)
        .def("__repr__", &Predictor::to_string);

    declare_index_class<TimeIndex::numeric_type, TimeIndex::unit>(m, "TimeIndex");
    declare_index_class<FreqIndex::numeric_type, FreqIndex::unit>(m, "FreqIndex");
    declare_index_class<PhaseIndex::numeric_type, PhaseIndex::unit>(m, "PhaseIndex");

    py::class_<Candidate>(m, "Candidate")
    .def_property_readonly("pred", &Candidate::pred)
    .def_property_readonly("time_index", &Candidate::time_index)
    .def_property_readonly("phase_index", &Candidate::phase_index)
    .def_property_readonly("freq_index", &Candidate::freq_index)
    .def_property_readonly("nsub", &Candidate::nsub)
    .def_property_readonly("nband", &Candidate::nband)
    .def_property_readonly("nbin", &Candidate::nbin)
    .def(
        "data", 
        [] (const Candidate& self) {
            return py::array_t<float, py::array::c_style>(
                {self.nsub(), self.nband(), self.nbin()}, &self.data()[0]
            );
        }
    );

    declare_tfblock<uint8_t>(m, "uint8");
    declare_fold<uint8_t>(m, "uint8");
    declare_benchmark_fold<uint8_t>(m, "uint8");
}
