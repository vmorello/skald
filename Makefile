.DEFAULT_GOAL := help
PKG = skald
LIBFOLD_DIR = skald/cpp

install: uninstall libfold ## Perform a fresh install of the package in development mode
	pip install -e .

libfold: ## Rebuild the libfold C++ extension
	make -C $(LIBFOLD_DIR) clean libfold

uninstall: clean ## Uninstall the package
	pip uninstall -y $(PKG)

clean: ## Remove temporary and build files
	rm -rf build/
	rm -rf tmp/

# GLORIOUS hack to autogenerate Makefile help
# This simply parses the double hashtags that follow each Makefile command
# https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Print this help message
	@echo "Makefile help for ${PKG}"
	@echo "===================================================================="
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: libfold install uninstall clean help