# skald

A proof-of-concept CPU-based parallel candidate folding code for the SKA pulsar search. It is written as a C++ header-only library, with python bindings generated with [pybind11](https://pybind11.readthedocs.io/en/stable/index.html). The main purpose of this software is to estimate how fast candidate folding can be done on the CPU.


## Requirements

The module requires python3 and has the following dependencies:  
* `pybind11`
* `numpy`
* `matplotlib`
* `astropy`
* `pandas`

You can either pre-install them with the package manager of your choice, or let `pip` do it (see below).

## Installation

After cloning the repository, simply run the following command in its base directory:
```
make install
```

This pulls required python modules via `pip`, builds the C++ source into a python extension `libfold`, and installs the module in development mode. If you wish to use a specific C++ compiler, specify it with the `CXX` environment variable. For example, to use the Intel C compiler:

```
CXX=icc make install
```

To rebuild the C++ source from scratch, simply run `make install` again.


## Applications

The module includes the following command-line python apps:

* `skald_fold`: Fold a single candidate from a SIGPROC filterbank file, saves the output as a numpy array and a JSON file containing the candidate parameters.
* `skald_gridfold`: Fold a grid of candidates in nu - nudot space from a SIGPROC Filterbank file. Saves a 5-dimensional numpy array, and a plot showing the sub-integrations of each candidate arranged on a 2-dimensional grid (nu on the X axis, nudot on the Y axis).
* `skald_benchmark`: Benchmark the code on a large block of arbitrary data. Print the total runtime and number of additions per second (APS).
* `skald_benchmark_suite`: Run the suite of benchmarks defined in AT4-524. Saves the output as a set of CSV files and generates summary PNG plots as well.

The file `benchmark_utils.py` contains the base functions used to measure performance on a trial grid of parameters (number of samples, number of channels, number of threads, etc.).


## Using the C++ library directly

The C++ classes and functions can be imported from python as follows, and each have a detailed docstring:  

```python
from skald.libfold import Predictor, TFBlock_uint8, fold_uint8, benchmark_fold_uint8
```

A `Predictor` defines the phase model parameters and dimensions of a Candidate to be folded. `TFBlock_uint8` wraps a numpy array containing time-frequency data. `fold_uint8` takes a TFBlock and multiple predictors as arguments, and returns a list of `Candidate` objects that each wrap a folded data cube. `benchmark_fold_uint8` generates arbitrary TF data with specified dimensions, folds a list of user-defined predictors, and measures the total folding time.
